package hillel.inheritance.persons;

import java.util.Objects;

public class Citizen extends Person {

    private String country;

    private long personalNumber;

    private Passport passport;

    public Citizen() {
    }

    public Citizen(String sex,
                   String name,
                   String secondName,
                   int age,
                   String country,
                   long personalNumber,
                   Passport passport) {
        super(sex, name, secondName, age);
        this.country = country;
        this.personalNumber = personalNumber;
        passport.setName(getName());
        passport.setSecondName(getSecondName());
        this.passport = passport;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public long getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(long personalNumber) {
        this.personalNumber = personalNumber;
    }

    public Passport getPassport() {
        return passport;
    }

    public void setPassport(Passport passport) {
        passport.setName(getName());
        passport.setSecondName(getSecondName());
        this.passport = passport;
    }

    @Override
    public String toString() {
        return super.toString() + "=" + "Citizen{" +
                "country='" + country + '\'' +
                ", personalNumber=" + personalNumber +
                ", passport=" + passport +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Citizen)) return false;
        Citizen citizen = (Citizen) o;
        return getPersonalNumber() == citizen.getPersonalNumber() &&
                Objects.equals(getCountry(), citizen.getCountry()) &&
                Objects.equals(getPassport(), citizen.getPassport());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCountry(), getPersonalNumber(), getPassport());
    }
}
