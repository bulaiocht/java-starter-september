package hillel.inheritance.persons;

import java.time.LocalDate;

public class PersonRunner {
    public static void main(String[] args) {

        Citizen citizen = new Citizen();
        citizen.setPersonalNumber(325897564);
        citizen.setCountry("Ukraine");
        citizen.setAge(16);
        citizen.setSex("combat helicopter");
        citizen.setName("Bubble");
        citizen.setSecondName("Bubbleson");
        Passport passport = new Passport();

        passport.setNumber("HH28170");
        passport.setDateOfIssuing(LocalDate.of(2020, 10, 2));
        citizen.setPassport(passport);

        Citizen citizen1 = new Citizen();
        citizen1.setPersonalNumber(325897564);
        citizen1.setCountry("Ukraine");
        citizen1.setAge(16);
        citizen1.setSex("combat helicopter");
        citizen1.setName("Bubble");
        citizen1.setSecondName("Bubbleson");
        Passport passport1 = new Passport();

        passport1.setNumber("HH28170");
        passport1.setDateOfIssuing(LocalDate.of(2020, 10, 2));
        citizen1.setPassport(passport1);

        System.out.println(citizen);
        Object object = new Object();

        System.out.println(citizen.equals(citizen1));

        System.out.println(citizen.hashCode());
        System.out.println(citizen1.hashCode());

//        Citizen citizen2 = new Citizen();
//        Object o = new Object();
//
//        System.out.println(o instanceof Citizen);
//        System.out.println(citizen2 instanceof Citizen);
//        System.out.println(citizen2 instanceof Person);
//        System.out.println(citizen2 instanceof Object);
    }
}
