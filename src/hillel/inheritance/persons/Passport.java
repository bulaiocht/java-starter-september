package hillel.inheritance.persons;

import java.time.LocalDate;
import java.util.Objects;

public class Passport {

    private LocalDate dateOfIssuing;

    private String name;

    private String secondName;

    private String number;

    public Passport() {
    }

    public Passport(LocalDate dateOfIssuing, String name, String secondName, String number) {
        this.dateOfIssuing = dateOfIssuing;
        this.name = name;
        this.secondName = secondName;
        this.number = number;
    }

    public LocalDate getDateOfIssuing() {
        return dateOfIssuing;
    }

    public void setDateOfIssuing(LocalDate dateOfIssuing) {
        this.dateOfIssuing = dateOfIssuing;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Passport{" +
                "dateOfIssuing=" + dateOfIssuing +
                ", name='" + name + '\'' +
                ", secondName='" + secondName + '\'' +
                ", number='" + number + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Passport)) return false;
        Passport passport = (Passport) o;
        return Objects.equals(getDateOfIssuing(), passport.getDateOfIssuing()) &&
                Objects.equals(getName(), passport.getName()) &&
                Objects.equals(getSecondName(), passport.getSecondName()) &&
                Objects.equals(getNumber(), passport.getNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDateOfIssuing(), getName(), getSecondName(), getNumber());
    }
}
