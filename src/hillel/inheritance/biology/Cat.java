package hillel.inheritance.biology;

public class Cat extends Mammal {

    private String catName;

    public Cat() {
        super();
        System.out.println("Cat has been created");
    }

    public Cat(String catName, int age) {
        this.catName = catName;
        super.setAge(age);
    }

    public void miao(){
        System.out.println("Miao!");
    }

    @Override
    public void breath(){
        System.out.println("I purrrrrrrrr");
    }

    @Override
    public String toString() {
        return "Cat{" +
                "catName='" + catName + '\'' +
                ", age=" + getAge() +
                '}';
    }
}
