package hillel.inheritance.biology;

public class Animals {

    public Animals (){
        super();
        System.out.println("Animal object has been created");
    }

    public void eatOrganics(){
        System.out.println("I eat organics");
    }

    public void breed(){
        System.out.println("I breed");
    }

    public void breath(){
        System.out.println("I breath");
    }

}
