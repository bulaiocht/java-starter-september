package hillel.inheritance.biology;

public class InheritanceRunner {

    public static void main(String[] args) {
//        Animals animals = new Animals();
//        animals.breath();
//        animals.breed();
//        animals.eatOrganics();

//        Fish fish = new Fish();
//        fish.waterLife();
//        fish.breath();
//        fish.breed();
//        fish.eatOrganics();
//
//        Mammal mammal = new Mammal();
//        mammal.blood();
//        mammal.drinkMilk();
//        mammal.breath();
//        mammal.breed();
//        mammal.eatOrganics();

        Cat cat = new Cat();
        cat.miao();
        cat.blood();
        cat.eatOrganics();
        cat.breath();

        System.out.println(cat.toString());

        Cat cat1 = new Cat("Tomas", 8);
        System.out.println(cat1.toString());


    }
}
