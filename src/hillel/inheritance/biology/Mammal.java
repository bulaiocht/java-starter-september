package hillel.inheritance.biology;

public class Mammal extends Animals {

    private int age;

    public Mammal(){
        super();
        System.out.println("Mammal object has been created");
    }

    public void drinkMilk(){
        System.out.println("I drink milk when I small");
    }

    public void blood(){
        System.out.println("My blood is warm");
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public void breath() {
        super.breath();
        System.out.println("With air oxygen");
    }
}
