package hillel.inheritance.biology;

public class Fish extends Animals {

    public Fish(){
        System.out.println("Fish object has been created");
    }

    public void waterLife(){
        System.out.println("I live in the water");
    }

}
