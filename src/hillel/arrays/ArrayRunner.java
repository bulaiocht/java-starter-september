package hillel.arrays;

public class ArrayRunner {

    public static void main(String[] args) {

        int [] array1 = new int[10];
        
        //Deleted some spaces. Sorry.
        array1[0] = 2;

        array1[0] = 'v';

        int a = array1[0];

        int [] array2 = {10, 3, 5, 6, 8, 12};

        int [] secondLink = array2;

        for (int i = 0; i < array2.length; i++){
            System.out.print(array2[i] + " ");
        }

        array2[2] = 100500;

        System.out.println();

        for (int i = 0; i < secondLink.length; i++){

            System.out.print(secondLink[i] + " ");

        }
        System.out.println();

        int [] nullLink = null;

//        int i = nullLink[10];

//        int i = array2[12];

        for (int i : array2) {
            System.out.println(i);
        }

    }

}
