package hillel.hw;

import java.util.Arrays;
import java.util.Random;

public class ArraysHW {

    public static void main(String[] args) {

        int [] ages = new int [1000];

        Random random = new Random();

        int low = 12;
        int high = 90;
        for (int i = 0; i < ages.length; i++) {
            ages[i] = random.nextInt(high - low) + low;
        }

        int adultCount = 0;
        int pensionerCount = 0;
        int teenagerCount = 0;

        for (int age : ages) {
            if ((age) >= 16){
                adultCount++;
            }

            if (age >= 60){
                pensionerCount++;
            }

            if (age < 16) {
                teenagerCount++;
            }
        }

//        System.out.println("There are " + adultCount + " adults");
//        System.out.println("There are " + pensionerCount + " pensioners");
//        System.out.println("There are " + teenagerCount + " teenagers");
//        System.out.println("Teens to pensioner ratio is " + (double)teenagerCount/pensionerCount);


        int a = 'a';
        int z = 'z';

        char [] letters = new char[26];

        for (int i = 0; i < letters.length; i++) {
            letters[i] = (char)(random.nextInt(z - a) + a);
        }

        System.out.println(Arrays.toString(letters));

        Arrays.sort(letters);

        System.out.println(Arrays.toString(letters));

        int uniqueCount = 1;
        char currentLetter = letters[0];
        int letterCount = 1;
        for (int i = 0; i < letters.length - 1; i++) {
            if (currentLetter == letters[i + 1]){
                letterCount++;
                continue;
            } else {
                System.out.println("Letter " + (char)currentLetter + " occurs " + letterCount + " times");
                uniqueCount++;
            }
            currentLetter = letters[i + 1];
            letterCount = 1;
        }
        System.out.println("Letter " + (char)currentLetter + " occurs " + letterCount + " times");
        System.out.println(uniqueCount);

        char [] uniqueLetters = new char[uniqueCount];

        String myName = "Vitalii";
        char [] nameLetters = myName.toCharArray();



    }



}
