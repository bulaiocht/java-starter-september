package hillel.objects;

import hillel.modifiers.ModClass;
import hillel.modifiers.ModifierRunner;

public class ObjectsRunner {
    public static void main(String[] args) {

//        Food food = new Food();
//        food.callories = 500;
//        food.name = "Potatoes";
//        food.sweetness = 2.5;
//        food.scoville = 0;
//        food.fat = 1;
//        food.carbs = 20;
//        food.sodium = 5;
//        food.mass = 200;
//
//        food.sayMyName();
//
//        System.out.println("Food: callories=" + food.callories +
//                ", name=" + food.name + ", sweetness=" + food.sweetness);
//
//        Food food1 = new Food();
//        System.out.println(food1.name);
//
//        Food food2 = new Food("Tomatoes");
//        System.out.println(food2.name);

//        Test test = new Test();
//
//        Test test1 = new Test("Test text");
//
//        double result =  test.divide(3, 7);
//
//        System.out.println(result);

        Point pointA = new Point(3.5, 2.8);
        Point pointB = new Point(0, 7);
        double distance = pointA.distanceTo(pointB);
        System.out.println("Distance between points is: " + distance);

        int number = 5;
        System.out.println("Factorial of " + number + " is " + factorial(number));

        int num = 5;
        System.out.println("Factorial of " + num + " is " + fact(num));

        ModClass modClass = new ModClass();


    }

    public static long factorial(int number){
        if (number == 0) return 1;
        long result = 1;
        for (int i = number; i >= 1; i--) {
            result *= i;
        }
        return result;
    }

    public static long fact (int number){
        if (number == 0) return 1;
        return number * fact(number - 1);
    }

}
