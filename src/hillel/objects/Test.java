package hillel.objects;

public class Test {
    int number = 2;

    public Test(){
        System.out.println("Default constructor");
    }

    public Test(String text){
        System.out.println(text);
    }

    public int divide (int number){
        return number / this.number;
    }

    public double divide(int a, int b) {
        return (double) a / b;
    }

    public int divide (double a, int b){
        return (int) a / b;
    }

    public int divide (int a, double b){
        return a / (int) b;
    }
}
