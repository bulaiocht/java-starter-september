package hillel.objects;

public class Food {

    int scoville;

    double sweetness;

    int callories;

    int fat;

    int carbs;

    int sodium;

    String name;

    int mass;
    //No arg constructor
    public Food (){
        scoville = 1;
        sweetness = 1;
        callories = 1;
        fat = 1;
        carbs = 1;
        sodium = 1;
        name = "Product";
        mass = 1;
    }

    //One arg constructor
    public Food(String name){
        this.name = name;
    }


    public void sayMyName() {
        System.out.println(name);
    }

}
