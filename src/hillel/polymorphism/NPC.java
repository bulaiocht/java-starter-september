package hillel.polymorphism;

import java.util.Objects;

public class NPC extends AbstractNPC{

    private int health;
    private int x;
    private int y;

    public NPC() {
        this.health = 100;
    }

    public NPC(int health, int x, int y) {
        this.health = health;
        this.x = x;
        this.y = y;
    }

    public void speak(){
        System.out.println("Blah-blah!");
    }

    public void move(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void action(NPC npc){
        System.out.println("Ahaaa!");
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "NPC{" +
                "health=" + health +
                ", x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NPC)) return false;
        NPC npc = (NPC) o;
        return getHealth() == npc.getHealth() &&
                getX() == npc.getX() &&
                getY() == npc.getY();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHealth(), getX(), getY());
    }

    @Override
    public void killNPC() {
        this.health = 0;
    }
}
