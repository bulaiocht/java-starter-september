package hillel.polymorphism;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;

public class NpcRunner {
    public static void main(String[] args) {

//        NPC npc = new NPC();
//        npc.speak();
//        npc.move(0 ,3);
//        npc.action(new NPC());

//        NPC n1 = new Doc();
//        NPC n2 = new Private();
//        NPC n3 = new Officer();
//
//        NPC [] npcs = {n1, n2, n3};
//
//        printInfo(npcs);
//
//        NPC nd1 = new Doc();
//        NPC nd2 = new Doc();
//        NPC nd3 = new Doc();
//
//        printInfo(nd1, nd2, nd3);
//        killAll(nd1, nd2, nd3);
//        killAll(npcs);

//        AbstractNPC abstractNPC = new AbstractNPC();

        AbstractNPC abstractNPC = new AbstractNPC() {
            @Override
            public void killNPC() {
                System.out.println(getClass().getName());
                System.out.println("Killing NPC");
            }
        };
    }

    private static void printInfo(NPC...npcs){

        for (NPC n : npcs) {
            System.out.println(n);
            n.speak();
            n.move(0 ,0);
            n.action(new NPC());
            Doc d = (Doc)n;
            d.getNPCsHealth(new NPC());
            System.out.println();
        }
    }

    private static void printInfo(Object...objs){
        for (Object n : objs) {
            System.out.println(n);
            System.out.println();
        }
    }

    private static void printInfo(Object o1, Object o2, Object o3){}

    private static void printInfo(Object o1, Object o2){}

    private static void killAll(AbstractNPC...npcs){
        for (AbstractNPC npc : npcs) {
            npc.killNPC();
        }
    }

}
