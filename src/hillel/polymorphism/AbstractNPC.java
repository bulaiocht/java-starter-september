package hillel.polymorphism;

public abstract class AbstractNPC {

    public static final String constantString = "Arrrgggh!";

    protected int protectedField = 12;

    public void method(){
        System.out.println("method");
    }

    public abstract void killNPC();

}
