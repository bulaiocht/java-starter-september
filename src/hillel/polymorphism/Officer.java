package hillel.polymorphism;

public class Officer extends NPC {

    @Override
    public void speak() {
        System.out.println("Listen to my command!");
    }

    @Override
    public void move(int x, int y) {
        System.out.println("Negative!");
    }

    @Override
    public void action(NPC npc) {
        System.out.println("Go to the base, private!");
        npc.move(0, 0);
    }
}
