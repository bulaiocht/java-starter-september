package hillel.polymorphism;

public class Private extends NPC {

    @Override
    public void speak() {
        System.out.println("Sir, yes sir!");
    }

    @Override
    public void move(int x, int y) {
        System.out.println("Roger that!");
        super.move(x, y);
    }

    @Override
    public void action(NPC npc) {
        int health = npc.getHealth();
        int h = health - 15;
        npc.setHealth(h);
    }
}