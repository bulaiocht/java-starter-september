package hillel.interfaces;

public interface Writable {

    void write();

}