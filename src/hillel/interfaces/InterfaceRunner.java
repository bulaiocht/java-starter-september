package hillel.interfaces;

public class InterfaceRunner {
    public static void main(String[] args) {
        Implementor impl = new Implementor();
        impl.draw();
        impl.write();
        impl.defaultMethod();

        Drawable d = new Implementor();
        Drawable d1 = new ShapeDrawer();

        Writable w = new Implementor();

        d.draw();
        d.defaultMethod();
        d1.draw();
        d1.defaultMethod();

        w.write();

    }
}
