package hillel.interfaces;

public class Implementor implements Drawable, Writable {

    @Override
    public void draw() {
        System.out.println("I'm drawing");
    }

    @Override
    public void write() {
        System.out.println("I'm writing");
    }
}