package hillel.interfaces;

public interface Drawable {

    String CONSTANT = "Constant field";
    int NUMBER = 256;

    void draw();

    default void defaultMethod(){
        System.out.println("Default method");
    }

}
