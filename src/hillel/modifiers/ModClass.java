package hillel.modifiers;

public class ModClass {

    private double age;

    private String name;

    public void setAge(double newAge){
        this.age = newAge;
    }

    public double getAge(){
        return this.age;
    }

    public int getIntAge(){
        return (int) this.age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = validateName(name);
    }

    public void setName(int number){
        this.name = String.valueOf(number);
    }

    private String validateName(String name){
        if (name.matches("[a-zA-Z]*")){
            return name;
        }
        return "DUMMY";
    }

    void packagePrivateMethod(){

    }

    public final void finalMethod(){

    }
}
