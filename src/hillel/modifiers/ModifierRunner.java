package hillel.modifiers;

import java.util.Random;

public class ModifierRunner {

    public static String stringField = "THis class name is ModifierRunner";

    public static void main(String[] args) {

        final int a = 0;

//        a = 1;

        final ModClass modClass = new ModClass();
        ModClass modClass1 = new ModClass();
        modClass1 = modClass;

        final int [] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = new Random().nextInt(10);
        }

        String name = modClass.getName();

        modClass.setName(1);
        modClass.setAge(12.5);

        System.out.println("Name: " + modClass.getName()
        + ", age: " + modClass.getAge());

        double studentAge = modClass.getAge();

        getSomeAge(modClass.getIntAge());

    }

    public static void getSomeAge(int age){
        System.out.println(age);
    }

}