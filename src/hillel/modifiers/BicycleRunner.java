package hillel.modifiers;

public class BicycleRunner {

    public static void main(String[] args) {

        System.out.println(Bicycle.getCounter());

        for (int i = 0; i < 100; i++) {
            new Bicycle("Aistenok", 1);
        }

        for (int i = 0; i < 100; i++) {
            new Bicycle("Gorni lisapet", 18);
        }

        System.out.println(Bicycle.getCounter());

    }
}
