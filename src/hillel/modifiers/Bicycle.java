package hillel.modifiers;

public class Bicycle {

    private static int counter;

    private int gears;

    private String bicycleName;

    public Bicycle(String bicycleName, int gears) {
        counter++;
        this.gears = gears;
        this.bicycleName = bicycleName;
    }

    public static int getCounter() {
        return counter;
    }

    public String getBicycleName() {
        return bicycleName;
    }
}
