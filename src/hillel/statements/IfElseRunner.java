package hillel.statements;

import java.util.Scanner;

public class IfElseRunner {

    public static void main(String[] args) {

//        int number = 9;
//
//        int result = (int) Math.sqrt(number);
//
//        if ( result % 2 != 0 ){
//            System.out.println("Uneven number");
//        } else {
//            System.out.println("Even number");
//        }

//        int dynamicNumber = 100;
//
//        for ( int i = 0; i <= dynamicNumber ; i++ ){
//
//            if (i % 2 == 0){
//                System.out.println(i + " is an even number");
//            } else {
//                System.out.println(i + " is uneven number");
//            }
//
//        }
//        boolean stopFlag = true;
//        int i = 0;
//
//        for ( ; ; ){
//
//            if (i == 10){
//                System.out.println(i);
//                return;
//            }
//
//            if (i == 120){
//                System.out.println(i);
//                continue;
//            }
//
//            if (i == 250){
//                System.out.println("Ok computer");
//                break;
//            }
//            i++;
//
//        }
//
//        System.out.println("I'm outside for-loop");

//        do {
//            System.out.println("Condition is false");
//        } while(false);

//        Scanner scanner = new Scanner(System.in);
//
//        while (true){
//
//            System.out.println("Type something or \"quit\":");
//
//            String input = scanner.next();
//
//            if ("quit".equalsIgnoreCase(input)){
//
//                System.out.println("Bye bye!");
//                break;
//
//            }
//            System.out.println("You've typed: " + input);
//
//        }


        String dayName = "Thursday";

        switch (dayName){
            case "Monday":
                System.out.println(1 + " day of the week.");
                break;
            case "Tuesday":
                System.out.println(2 + " day of the week.");
                break;
            case "Wednesday":
                System.out.println(3 + " day of the week.");
                break;
            case "Thursday":
                System.out.println(4 + " day of the week.");
                break;
            case "Friday":
                System.out.println(5 + " day of the week.");
                break;
            case "Saturday":
                System.out.println(6 + " day of the week.");
                break;
            case "Sunday":
                System.out.println(7 + " day of the week.");
                break;
            default:
                System.out.println("No such day");
                break;
        }

    }

}
