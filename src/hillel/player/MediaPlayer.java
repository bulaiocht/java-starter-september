package hillel.player;

import java.util.function.Function;

public class MediaPlayer {

    public static void main(String[] args) {
//        Playable p1 = new MP3();
//        Playable p2 = new WAV();
//        Playable p3 = new MP4();
//
//        Decoder d1 = new MP3Decoder();
//        Decoder d2 = new WAVDecoder();
//        playFile(p1, d1);
//        playFile(p2, d2);
//        playFile(p3, d2);

        Function <String, Integer> f = (String s) -> s.length();
        System.out.println(f.apply("Hello World"));

        Playable playable = () -> {
            String s = String.valueOf(10);
            return s;
        };

    }
    private static void playFile(Playable playable,
                                 Decoder decoder){
        decoder.play(playable);
    }
}
