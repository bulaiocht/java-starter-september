package hillel.player;

public class WAVDecoder implements Decoder {

    @Override
    public void play(Playable playable) {
        if (playable instanceof WAV){
            System.out.println("Playing " + playable.getData());
        } else {
            System.out.println("Can't play this format");
        }
    }

    @Override
    public void pause(Playable playable) {
        if (playable instanceof WAV){
            System.out.println("Pausing " + playable.getData());
        } else {
            System.out.println("Can't play this format");
        }

    }

    @Override
    public void forward(Playable playable) {
        if (playable instanceof WAV){
            System.out.println("Forwarding " + playable.getData());
        } else {
            System.out.println("Can't play this format");
        }

    }

    @Override
    public void backward(Playable playable) {
        if (playable instanceof WAV){
            System.out.println("Rewinding " + playable.getData());
        } else {
            System.out.println("Can't play this format");
        }

    }

}
