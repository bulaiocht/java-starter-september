package hillel.player;

public interface Decoder {

    void play(Playable playable);

    void pause(Playable playable);

    void forward(Playable playable);

    void backward(Playable playable);
}
