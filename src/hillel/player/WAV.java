package hillel.player;

public class WAV implements Playable {

    private static final String data = "WAV byte stream";

    @Override
    public String getData() {
        System.out.println("Passing " + data);
        return data;
    }
}
