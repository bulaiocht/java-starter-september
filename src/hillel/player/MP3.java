package hillel.player;

public class MP3 implements Playable {

    private static final String data = "MP3 byte stream";

    @Override
    public String getData() {
        System.out.println("Passing " + data);
        return data;
    }
}
