package hillel.datatype.primitive;

import java.math.BigDecimal;
import java.math.BigInteger;

public class PrimitiveRunner {

    public static void main(String[] args) {

        boolean booleanVariable = false; //false

        byte byteVariable = 127;

        char charVariable = '?';

        char anotherChar = 1280;

        short shortValue = 1024;

        int integerValue = 9_000_000;

        long longValue = 90_000_000L;

        float floatValue = 365.78901f;

        double doubleValue = 356.0d;

        int binary = 0b001001;

        int octal = 0753;

        int hexadecimal = 0xFFE333;

        char someChar = '\uF25E';

        int bigBucket = 125654;

        byte smallBucket = 125;

        smallBucket = (byte) bigBucket;

        System.out.println(binary);

        System.out.println(hexadecimal);

        System.out.println(smallBucket);

        System.out.println(bigBucket + " as binary: " + Integer.toBinaryString(bigBucket));

        System.out.println((byte) 0b11010110);

    }

}
