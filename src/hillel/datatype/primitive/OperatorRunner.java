package hillel.datatype.primitive;

import java.util.Random;

public class OperatorRunner {

    public static void main(String[] args) {

        int a = 5;

        int b = 2;

        System.out.println(a != b);

        System.out.println(true || false);

        System.out.println(true && false);

        System.out.println((a + b) > 0 && (a - b) != 0);

        b = a % 2 != 0 ? 5 : 17;

        a += 2;

        a = a + 2;

//         a++; //a = a + 1;
//
//        System.out.println(a);
//
//        a--;
//
//        System.out.println(a);
//
//        ++a;
//
//        System.out.println(a);
//        System.out.println(Integer.toBinaryString(a));
//        System.out.println(Integer.toBinaryString(~a));
//        System.out.println(!false);
//        System.out.println(4/2);
//        System.out.println(4*2);
//        System.out.println(4%2);
//        System.out.println(4 + 7);
//        System.out.println(2 - 11);
//
//        System.out.println(4/5);
//
//        System.out.println(4.0f/5.0f);
//
//        int b = 1;
//
//        System.out.println(Integer.toBinaryString(b));
//        System.out.println(Integer.toBinaryString(b >>> 32));
//        System.out.println(b >>> 32);
//
//        System.out.println(Integer.toBinaryString(b >> 32));
//        System.out.println(b >> 32);

        double result = Math.pow(25, 2);
        double res = Math.abs(-265);
        System.out.println(result);
        System.out.println(res);
        System.out.println(Math.sqrt(25));
        double random = Math.random();
        System.out.println (random);

        Random otherRandom = new Random();
        System.out.println(otherRandom.nextInt(10));

        System.out.println((2 + 3) * 4);
    }

}
